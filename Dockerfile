FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    build-essential \
    gcc \
    libc6-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY ./database/database.c .
COPY ./client/client.c .

RUN gcc -o database database.c
RUN gcc -o client client.c

COPY run.sh .
RUN chmod +x run.sh

CMD ["./run.sh"]
