
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <stdbool.h>
#define PORT 8080
char user[1024];
int main(int argc, char *argv[]){
     char *usr = argv[2];
    char *pswd = argv[4];
 if (argc != 5) {
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int sock = 0, valread;
    struct sockaddr_in addr;
    char buffer[2049]= {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Socket creation error\n");
        exit(EXIT_FAILURE);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr) <= 0) {
        printf("Invalid address/ Address not supported\n");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        printf("Connection Failed 47\n");
        exit(EXIT_FAILURE);
    }
    int root=0;
  if (geteuid() == 0) {
        root=1;
    }
    char authenticate[100];
    if(root){
        sprintf(authenticate, "access Root 0");
    }else{
        sprintf(authenticate, "access %s %s", usr, pswd);
    }
    

    printf("Launcher %s\n", authenticate);
    send(sock, authenticate, strlen(authenticate), 0);

    memset(buffer, 0, sizeof(buffer));
    valread = recv(sock, buffer, sizeof(buffer), 0);
    printf("Response from server: %s\n", buffer);

    memset(buffer, 0, sizeof(buffer));
	char sql[2049] = {0};
    while(1){
      
        printf("Acces[%s]> ", user);
        fgets(sql, sizeof(sql), stdin);
        printf("Client cmd: %s\n", sql);
        send(sock, sql, strlen(sql), 0);


        memset(buffer, 0, sizeof(buffer));
        valread = recv(sock, buffer, sizeof(buffer), 0);
        printf("%s\n", buffer);
		if(strcmp(sql, "EXIT")==0){
			break;
		}


    }
    close(sock);
    return 0;
}