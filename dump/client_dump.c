#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

struct allowed {
    char name[10000];
    char password[10000];
};

int cekperm(char *usr, char *db) {
   if (strcmp(usr, "Root") == 0)
        return 1; // Root has access to all databases

    FILE *file = fopen("../database/perm.csv", "r");
    if (file != NULL) {
        char line[256];
        char curusr[1024];
        char curdb[1024];

        while (fgets(line, sizeof(line), file)) {
            sscanf(line, "%[^,],%s", curusr, curdb);

            if (strcmp(curusr, usr) == 0 && strcmp(curdb, db) == 0) {
                fclose(file);
                return 1; // User has access rights to the database
            }
        }

        fclose(file);
    }

}
int main(int argc, char *argv[]){
 if (argc != 5) {
        printf("Usage: %s -u [username] -p [password] [database]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printf("Usage: %s -u [username] -p [password] [database]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    int allowed = 0;
    if (geteuid() == 0) {
        allowed = 1;
    } else {
        allowed = cekperm(argv[2], argv[5]);
    }
    if (allowed == 0) {
        return 0;
    }
    int ch;
    FILE *fp, *dp;
    fp = fopen("../database/log.txt", "r");
    dp = fopen("command.backup.txt", "w");
    if (fp == NULL) {
        printf("Failed to open log file.\n");
        return 0;
    }
    char buffer[20000];
    while (fgets(buffer, sizeof(buffer), fp)) {
        char *colon = strchr(buffer, ':');
        if (colon != NULL) {
            // Move the pointer forward to skip timestamp and username
            colon = strchr(colon + 1, ':');
            colon = strchr(colon + 1, ':');
            colon = strchr(colon + 1, ':');
            if (colon != NULL) {
                fprintf(dp, "%s", colon + 1);
            }
        }
    }
    fclose(fp);
    fclose(dp);
    return 0;
}

