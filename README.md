# sisop-praktikum-fp-2023-BS-D06
## Anggota
### 5025211119	Gilang Aliefidanto

### 5025211056	I Gusti Agung Ngurah Adhi Sanjaya

### 5025211185	Rano Noumi Sulistyo


## PENDAHULUAN
Pada final project ini kita harus membuat sebuah sistem database sederhana yang bisa beberapa hal, mulai dari server client, authorisasi, autentifikasi, DDL, DML, logging dan backup.
### client.c
Pada awalnya, program mendefinisikan sebuah konstanta bernama PORT dengan nilai 8080, yang akan digunakan sebagai nomor port untuk koneksi socket. Selanjutnya, program mendeklarasikan array karakter bernama user dengan panjang 1024, yang akan digunakan untuk menyimpan informasi pengguna.

Fungsi main adalah titik masuk eksekusi program. Program ini menerima argumen baris perintah argc dan argv yang digunakan untuk memeriksa jumlah dan format argumen yang diberikan saat menjalankan program. Jika jumlah argumen tidak sesuai dengan yang diharapkan (5 argumen) atau format argumen tidak benar, program akan mencetak pesan penggunaan yang menjelaskan cara menggunakan program dan kemudian keluar dari program.

Setelah melakukan pemeriksaan argumen, program akan membuat soket menggunakan fungsi socket(). Soket tersebut menggunakan alamat IPv4 dan protokol TCP. Selanjutnya, program menginisialisasi struktur sockaddr_in dengan alamat IP "127.0.0.1" dan nomor port yang telah ditentukan sebelumnya. Kemudian, program mencoba melakukan koneksi ke server menggunakan fungsi connect().
Program kemudian memeriksa apakah program berjalan sebagai root user dengan memeriksa nilai pengguna efektif menggunakan fungsi geteuid(). 

Selanjutnya, program membangun sebuah string authenticate yang akan dikirim ke server. Jika program berjalan sebagai root user, string tersebut akan berisi "access Root 0". Jika bukan root user, string tersebut akan berisi "access [username] [password]" yang diperoleh dari argumen baris perintah saat menjalankan program. String authenticate ini akan dicetak sebagai pesan Launcher.

Setelah itu, program mengirimkan data authenticate ke server menggunakan fungsi send() untuk otentikasi. Program kemudian menerima respons dari server menggunakan fungsi recv() dan mencetak pesan respons dari server.

Setelah tahap otentikasi selesai, program memasuki loop tak terbatas yang akan menerima input dari pengguna. Input tersebut akan dikirim ke server menggunakan fungsi send(). Jika pengguna memasukkan "EXIT" sebagai input, loop akan berhenti dan program akan menutup soket.
```

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <stdbool.h>
#define PORT 8080
char user[1024];
int main(int argc, char *argv[]){
     char *usr = argv[2];
    char *pswd = argv[4];
 if (argc != 5) {
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int sock = 0, valread;
    struct sockaddr_in addr;
    char buffer[2049]= {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Socket creation error\n");
        exit(EXIT_FAILURE);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr) <= 0) {
        printf("Invalid address/ Address not supported\n");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        printf("Connection Failed 47\n");
        exit(EXIT_FAILURE);
    }
    int root=0;
  if (geteuid() == 0) {
        root=1;
    }
    char authenticate[100];
    if(root){
        sprintf(authenticate, "access Root 0");
    }else{
        sprintf(authenticate, "access %s %s", usr, pswd);
    }
    

    printf("Launcher %s\n", authenticate);
    send(sock, authenticate, strlen(authenticate), 0);

    memset(buffer, 0, sizeof(buffer));
    valread = recv(sock, buffer, sizeof(buffer), 0);
    printf("Response from server: %s\n", buffer);

    memset(buffer, 0, sizeof(buffer));
	char sql[2049] = {0};
    while(1){
      
        printf("Acces[%s]> ", user);
        fgets(sql, sizeof(sql), stdin);
        printf("Client cmd: %s\n", sql);
        send(sock, sql, strlen(sql), 0);


        memset(buffer, 0, sizeof(buffer));
        valread = recv(sock, buffer, sizeof(buffer), 0);
        printf("%s\n", buffer);
		if(strcmp(sql, "EXIT")==0){
			break;
		}


    }
    close(sock);
    return 0;
}
```
## server
server sederhana menggunakan protokol TCP/IP. Server ini berfungsi untuk menerima koneksi dari klien, memproses perintah yang diterima, dan memberikan respons kepada klien.

Pertama, kode ini mendeklarasikan beberapa variabel yang akan digunakan dalam program. Variabel auth digunakan untuk mengontrol otorisasi pengguna. Awalnya, variabel ini diinisialisasi dengan nilai 0, yang menandakan bahwa pengguna belum terotentikasi.

Selanjutnya, terdapat tiga array karakter yang dideklarasikan, yaitu buffer, msg, dan query. buffer digunakan untuk menyimpan data yang diterima dari klien. msg digunakan untuk menyimpan pesan yang akan dikirim kembali ke klien sebagai respons. query digunakan untuk memproses perintah yang diterima dari klien.

Selanjutnya, kode ini membuat socket server menggunakan fungsi socket() dengan menggunakan alamat IPv4 dan jenis soket TCP. Jika pembuatan soket gagal, program akan menampilkan pesan kesalahan dan keluar dari program.

Setelah itu, soket server diikat ke alamat IP dan port yang ditentukan menggunakan fungsi bind(). Jika proses binding gagal, pesan kesalahan akan ditampilkan dan program akan keluar.

Selanjutnya, menggunakan fungsi listen(), server akan mulai mendengarkan koneksi dari klien. Jika proses listening gagal, pesan kesalahan akan ditampilkan dan program akan keluar.

Setelah itu, program akan mencetak pesan "Server started" yang menandakan bahwa server telah siap menerima koneksi dari klien.

Selanjutnya, menggunakan fungsi accept(), server akan menerima koneksi dari klien. Jika koneksi gagal, pesan kesalahan akan ditampilkan dan program akan keluar.

Setelah itu, program masuk ke dalam loop while (1), yang akan terus berjalan selama server aktif. Di dalam loop ini, server akan menerima data dari klien menggunakan fungsi read() dan menyimpannya dalam variabel buffer. Kemudian, variabel query akan diisi dengan isi dari buffer. Berdasarkan yang diread perintah perintah akan dapat dilakukan.
Proses menerima perintah dari klien dan memberikan respons akan terus berlanjut dalam loop hingga program dihentikan atau terjadi kondisi tertentu yang mengakibatkan keluarnya program.
```
int main(){
int auth=0;
 makedbdir();
 mkusers();
    char buffer[30030], msg[300303] = {0}, query[30030]={0};
    int server_fd, new_socket, vread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server started\n");

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
        perror("accept failed");
        exit(EXIT_FAILURE);
    }

    char user[1024]={0}, pswd[1024]={0};
while (1){
        memset(buffer, 0, sizeof(buffer));
        vread = read(new_socket, buffer, sizeof(buffer) - 1);
        memset(msg, 0, sizeof(msg));
        strcpy(query, buffer);
```
## autorisasi dan autentifikasi
Pertama kita akan membuat 2 file yang berisi users dan permission yang dimiliki oleh user tersebut. 
```
void mkusers(){
    FILE *userTableFile = fopen(usrfile, "a");
    if (userTableFile == NULL) {
        perror("Failed to create user table");
        exit(EXIT_FAILURE);
    }
    FILE *permTableFile = fopen(permfile, "a");
    if (permTableFile == NULL) {
        perror("Failed to create user table");
        exit(EXIT_FAILURE);
    }
}
```
Kedua file ini akan menjadi tulang punggung dari sistem autentifikasi dan autorisasi. Sebagai root kita bisa memasukkan user baru ke user file dan memberi permission kepada database tertentu di tabel permission.
Hal ini dilakukan dengan fungsi yang akan menulis user dn password ke file user bila user adalah root dan syntax sesuai dengan diminta. Syntax tersebut kali ini adalah CREATE USER [nama_user] IDENTIFIED BY [password_user]; 
Terdapat juga fungsi yang akan memberi permission terhadap database tertentu dan usernya, hal ini dapat dilakukan root dengan menulis syntax tertentu di client. Syntax tersebut adalah GRANT PERMISSION [nama_database] INTO [nama_user];
Ada juga fungsi yang mengecek apakah user ada dalam file user, jika ada akan mereturn 1;
Selain itu terdapat fungsi yang akan mengecek bila sebuah user memiliki permission terhadap database tertentu. Fungsi tersebut akan melihat data permission dan mereturn 1 jika ada dalam file. 
```
int checkusr(const char* usr) {
    FILE* file = fopen(usrfile, "r");
    if (file == NULL) {
        printf("File %s could not be opened.\n", usrfile);
        return 0;
    }

    char line[256];
    while (fgets(line, sizeof(line), file) != NULL) {
        if (strncmp(line, usr, strlen(usr)) == 0) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}

void crusr(const char* usr, const char* pswd) {
    FILE* file = fopen(usrfile, "a");
    if (file == NULL) {
        printf("File %s could not be opened.\n", usrfile);
        return;
    }

    fprintf(file, "%s %s\n", usr, pswd);
    fclose(file);
}
int mkperm(char *usr, char *db) {
    FILE *file = fopen(permfile, "a");
    if (file == NULL) {
        printf("File could not be opened.\n");
        return 0;
    }
        fprintf(file, "%s,%s\n", usr, db);
        fclose(file);
        return 1;
}
int checkperm(char *usr, char *db){
    if (strcmp(usr, "Root") == 0)
        return 1; // Root has access to all databases

    FILE *file = fopen(permfile, "r");
    if (file != NULL) {
        char line[256];
        char curusr[1024];
        char curdb[1024];

        while (fgets(line, sizeof(line), file)) {
            sscanf(line, "%[^,],%s", curusr, curdb);

            if (strcmp(curusr, usr) == 0 && strcmp(curdb, db) == 0) {
                fclose(file);
                return 1; // User has access rights to the database
            }
        }

        fclose(file);
    }

    return 0; // User does not have access rights to the database
}
```
, server akan memproses perintah yang diterima dari klien. Jika perintah mengandung kata "access", server akan melakukan autentikasi pengguna dengan memeriksa username dan password yang diberikan. Jika autentikasi berhasil, variabel auth akan diubah menjadi 1 dan pesan "Welcome [username]!" akan dicetak. Jika autentikasi gagal, pesan "Authentication failed!" akan dicetak.

Selain itu, jika perintah mengandung kata "CREATE USER", server akan memeriksa apakah pengguna yang meminta pembuatan pengguna baru memiliki otoritas yang cukup (misalnya, username "Root"). Jika otoritas cukup, server akan membuat pengguna baru dengan username dan password yang diberikan, dan pesan "Create username success" akan dicetak. Jika pengguna dengan username yang sama sudah ada, pesan "This username is already exist" akan dicetak.

Selanjutnya, jika perintah mengandung kata "GRANT PERMISSION", server akan memeriksa apakah pengguna yang meminta pemberian izin memiliki otoritas yang cukup. Jika otoritas cukup, server akan memberikan izin akses ke pengguna yang ditentukan terhadap database yang ditentukan, dan pesan "Grant Access [database] to [username]" akan dicetak. Jika pengguna yang dimaksud tidak ada, pesan "User does not exist" akan dicetak.
```
if(strstr(buffer, "access") != NULL){ 
            strcpy(msg, "Login detected");
            char *token = strtok(query, " ");;
            token = strtok(NULL, " ");
            strcpy(user, token);
            token = strtok(NULL, " ");
            strcpy(pswd, token);
            if(strcmp(user, "Root") == 0 || checkusr(user)== 1){
                auth =1;
                printf("Welcome %s!\n", user);
            }else{
                printf("Authentification failed!\n"); 
            }
        }
        else if(strstr(query, "CREATE USER") != NULL){ 
            if(strcmp(user, "Root")==0){
                char newuser[1024], newpswd[1024];

                char *token = strtok(query, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, " ");
                strcpy(newuser, token);
                if (token != NULL) {
                    printf("New user : %s\n", newuser);
                }
                token = strtok(NULL, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, ";");
                strcpy(newpswd, token);
                if (token != NULL) {
                    printf("New user password: %s\n", newpswd);
                }
                if(checkusr(newuser)){
                    printf("This username is already exist!\n");
                    strcpy(msg, "This username is already exist");
                }else{
                    crusr(newuser, newpswd);
                    strcpy(msg, "Create username success");
                }
            }else{
                strcpy(msg, "Not root, create failed");
            }else if(strstr(query, "GRANT PERMISSION")){ 
           if(strcmp(user, "Root") == 0){   
                char *tokBuf = strtok(query, " ");
                tokBuf = strtok(NULL, " ");
                char *dbAcc = strtok(NULL, " ");
                tokBuf = strtok(NULL, " ");
                char *userAcc = strtok(NULL, ";");

                if(checkusr(userAcc)){
                    mkperm(userAcc, dbAcc);
                    sprintf(msg, "Grant Acess %s to %s", dbAcc, userAcc);
                }else{
                    printf("user does not exist\n");
                    strcpy(msg, "User  not exist!");
                }
                }
```
## Penjelasan DDL
### CREATE DATABASE
 program melakukan pengecekan apakah string query mengandung kata kunci "CREATE DATABASE". Jika kata kunci tersebut ditemukan, program kembali memecah string query menjadi token-token menggunakan fungsi strtok(). Token ketiga yang berisi nama database yang akan dibuat akan diambil. Program kemudian memanggil fungsi mkdb() untuk membuat database dengan nama tersebut. Pesan "Invoke create database" akan disalin ke variabel msg. Jika pembuat database bukan "root", program akan memanggil fungsi mkperm() untuk memberikan izin akses kepada pengguna tersebut terhadap database yang baru dibuat. Selanjutnya, program juga akan memanggil fungsi mkperm() untuk memberikan izin akses kepada pengguna "Root" terhadap database tersebut.
```
if(strstr(query, "CREATE DATABASE") != NULL){ 
            char *tokBuf = strtok(query, " ");
        	tokBuf = strtok(NULL, " ");
        	tokBuf = strtok(NULL, " ");
        	char *database = strtok(tokBuf, ";");
            database[strlen(database)] = '\0';
        	mkdb(database);
            strcpy(msg, "Invoke create database");
            if(strcmp(user, "root") != 0){mkperm(user, database);} // jika pembuat bukan root
            mkperm("Root", database);
```
mkdb bertujuan untuk membuat sebuah direktori baru yang akan digunakan sebagai database.
Pada awalnya, kode ini menginisialisasi sebuah array karakter bernama dirPath dengan ukuran 100. Array ini akan digunakan untuk menyimpan jalur atau path dari direktori yang akan dibuat.

Selanjutnya, menggunakan fungsi sprintf, dbpath (yang diasumsikan sebagai jalur direktori utama) digabungkan dengan database (yang merupakan nama direktori yang ingin dibuat). Hasil penggabungan tersebut disimpan dalam dirPath.
Kemudian, kode ini menggunakan fungsi mkdir untuk membuat direktori baru dengan menggunakan dirPath sebagai path-nya. Angka 0777 yang ada pada parameter kedua mkdir merupakan mode atau hak akses untuk direktori yang baru dibuat.
Setelah itu, kode melakukan pemeriksaan terhadap status pengembalian dari fungsi mkdir. Jika nilai status sama dengan 0, artinya direktori berhasil dibuat. Maka, kode akan mencetak pesan "Database created: [nama_database]" menggunakan printf dan mengembalikan nilai 1, yang menandakan pembuatan database berhasil.
Namun, jika nilai status tidak sama dengan 0, artinya terdapat kegagalan dalam pembuatan direktori. Maka, kode akan mencetak pesan error dengan menggunakan perror dan mengembalikan nilai 0, yang menandakan pembuatan database gagal.
Dengan menggunakan kode di atas, kita dapat membuat direktori baru untuk menyimpan database dengan memanggil fungsi mkdb dan menyediakan nama database sebagai argumen.
```
int mkdb(char *database) {
    char dirPath[100];
    sprintf(dirPath, "%s/%s", dbpath, database);

    int status = mkdir(dirPath, 0777);
    if (status == 0) {
        printf("Database created: %s\n", database);
        return 1; // Database created successfully
    } else {
        perror("Failed to create database");
        return 0; // Database creation failed
    }
}
```
### USE DATABASE
Kode yang diberikan merupakan bagian dari suatu program yang berkaitan dengan manipulasi basis data. Pada bagian pertama kode, terdapat pengecekan apakah string query mengandung kata kunci "USE". Jika kata kunci tersebut ditemukan, program akan memecah string query menjadi token-token menggunakan fungsi strtok() dengan spasi sebagai pemisah. Token kedua yang berisi nama database yang ingin digunakan akan diambil. Setelah itu, program memeriksa izin akses pengguna terhadap database tersebut dengan memanggil fungsi checkperm(). Jika pengguna memiliki izin akses, maka database tersebut dianggap sebagai database yang sedang digunakan. Pesan "Database Accessed" akan dicetak dan disalin ke variabel msg. Jika pengguna tidak memiliki izin akses, program akan mencetak pesan "Access denied" dan menyimpan pesan "No authorization upon database!" ke variabel msg.
```
if(strstr(query, "USE") != NULL){
            char *tokBuf = strtok(query, " ");
        	tokBuf = strtok(NULL, " ");
        	char *database = strtok(tokBuf, ";");

        	if(checkperm(user, database) == 1){
        		strcpy(CURDATABASE, database);
                printf("Database Accessed\n");
                strcpy(msg, "Database accessed");
			}else{
				//tidak punya
				printf("Access denied\n");
                strcpy(msg, "No authorization upon database!");
			}
}
```
### CREATE TABLE
```
if(strstr(query, "CREATE TABLE") != NULL){
            if(strcmp(CURDATABASE, "") != 0){ 
                char tableName[1024], attr[1024];
                char *token = strtok(query, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, "(");
                strcpy(tableName, token);
                token = strtok(NULL, ")");
                strcpy(attr, token);
                createTable(tableName, attr);
                strcpy(msg, "Invoke create table");
            }
            else{
                printf("Use a database first to create table\n");
                strcpy(msg, "Use a databse first!");
            }
```
Pada bagian pertama kode, terdapat pengecekan apakah string query mengandung kata kunci "CREATE TABLE" menggunakan fungsi strstr(). Jika kata kunci tersebut ditemukan, program akan melanjutkan ke dalam blok ini.

Di dalam blok tersebut, program melakukan pengecekan apakah variabel CURDATABASE tidak kosong (berarti ada database yang sedang digunakan). Hal ini dilakukan dengan membandingkan CURDATABASE dengan string kosong menggunakan fungsi strcmp(). Jika kondisi tersebut terpenuhi, program melanjutkan ke langkah selanjutnya.

Program kemudian mendeklarasikan dua variabel lokal, yaitu tableName dan attr, yang berfungsi untuk menyimpan nama tabel dan atribut-atribut yang akan dibuat. Program menggunakan fungsi strtok() untuk memecah string query menjadi token-token yang dipisahkan oleh spasi. String tersebut akan sesuai syntax yang ditentukan, syntax tersebut adalah CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int); 
Token-token tersebut digunakan untuk mendapatkan nama tabel dan atribut-atribut dari string query. Nama tabel dan atribut-atribut tersebut disalin ke variabel tableName dan attr.

Selanjutnya, program memanggil fungsi createTable() dengan parameter tableName dan attr untuk membuat tabel dengan nama dan atribut yang sesuai. Pesan "Invoke create table" disalin ke variabel msg.

Namun, jika variabel CURDATABASE kosong, program mencetak pesan "Use a database first to create table" dan menyimpan pesan "Use a database first!" ke variabel msg. Hal ini menunjukkan bahwa pengguna harus terlebih dahulu menggunakan database sebelum dapat membuat tabel.
```
void extractAttributeNames(const char* inputString, char* outputString) {
    char attrBuf[1024];
    strcpy(attrBuf, inputString);

    char* token;
    char* savePtr;
    char* attributeName;

    token = strtok_r(attrBuf, ",", &savePtr);
    while (token != NULL) {
        // Find the first space character to separate attribute name
        attributeName = strtok(token, " ");
        if (attributeName != NULL) {
            strcat(outputString, attributeName);
            strcat(outputString, ",");
        }
        token = strtok_r(NULL, ",", &savePtr);
    }

    // Remove the trailing comma, if any
    if (strlen(outputString) > 0) {
        outputString[strlen(outputString) - 1] = '\0';
    }
    printf("Invoke extract attrnames, results: %s\n", outputString);
}

int createTable(char *table, char *rawAttributes) {
    char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");

    sprintf(dirPath, "%s/%s/%s.csv", dbpath, CURDATABASE, tableBuf);
    printf("create table: %s\n", dirPath);

    FILE *tableFile = fopen(dirPath, "a");

    if(tableFile != NULL){
        printf("Table created: %s\n", table);
        char attrNames[1024];
        extractAttributeNames(rawAttributes, attrNames);

        fprintf(tableFile, "%s\n", attrNames);
        fclose(tableFile);
        return 1; // Database created successfully

    } else {
        perror("Failed to create table");
        return 0; // Database creation failed
    }
}
```
Fungsi extractAttributeNames bertujuan untuk mengekstrak nama atribut dari sebuah string input (inputString) dan menyimpannya dalam string output (outputString). Pertama, kode ini menyalin string input ke dalam array karakter attrBuf menggunakan fungsi strcpy. Selanjutnya, menggunakan fungsi strtok_r, kode memecah attrBuf berdasarkan koma (,) dan melakukan iterasi melalui token-token yang dihasilkan. Dalam setiap iterasi, kode menggunakan strtok untuk memecah token menjadi nama atribut dengan menggunakan spasi (" ") sebagai pemisah. Jika nama atribut berhasil ditemukan, kode akan menambahkannya ke dalam string output menggunakan fungsi strcat, diikuti dengan tanda koma (",") sebagai pemisah antar atribut. Setelah selesai melakukan pemecahan untuk semua token, kode memeriksa apakah string output memiliki koma terakhir dan menghapusnya jika ada. Terakhir, kode mencetak string output menggunakan printf. String akan berupa header dari tabel yang bersyntax kolom1,kolom2,kolom3....

Fungsi createTable digunakan untuk membuat sebuah tabel dengan nama yang diberikan (table) dan atribut-atribut yang diberikan (rawAttributes). Pertama, kode menginisialisasi array karakter dirPath yang akan menyimpan jalur lengkap ke file CSV yang akan dibuat untuk tabel. Kode lalu membuat path yang sesuai berdasarkan path databases, CURDATABASE(database sekarang) dan nama tabel. Setelah persiapan jalur file selesai, kode mencoba membuka file dengan mode "a" (append) menggunakan fopen. Jika pembukaan file berhasil, kode mencetak pesan bahwa tabel telah berhasil dibuat menggunakan printf. Selanjutnya, kode memanggil fungsi extractAttributeNames untuk mendapatkan string yang berisi nama atribut dari rawAttributes dan menyimpannya dalam string attrNames. Kode kemudian menggunakan fprintf untuk menulis string attrNames ke dalam file tabel yang telah dibuka, diikuti dengan karakter baris baru. Terakhir, kode menutup file dan mengembalikan nilai 1 untuk menunjukkan bahwa pembuatan tabel berhasil. Jika pembukaan file gagal, kode mencetak pesan error menggunakan perror dan mengembalikan nilai 0 untuk menunjukkan bahwa pembuatan tabel gagal.
### DROP
Pada bagian pertama kode, terdapat pengecekan apakah string query mengandung kata kunci "DROP" menggunakan fungsi strstr(). Jika kata kunci tersebut ditemukan, program akan melanjutkan ke dalam blok ini.

Di dalam blok tersebut, program melakukan pengecekan lebih lanjut untuk memastikan jenis operasi DROP yang akan dilakukan. Terdapat tiga kemungkinan operasi DROP yang diperiksa: DROP DATABASE, DROP TABLE, dan DROP COLUMN.

Jika string query mengandung kata kunci "DROP DATABASE", program akan melanjutkan ke blok pertama dalam if statement ini. Program menggunakan fungsi strtok() untuk memecah string query menjadi token-token yang dipisahkan oleh spasi. Token-token tersebut digunakan untuk mendapatkan nama database yang akan dihapus. Nama database disalin ke variabel dropDB. Program memanggil fungsi checkperm() untuk memeriksa izin akses pengguna terhadap database tersebut. Jika pengguna memiliki izin akses, program memanggil fungsi dropdb() untuk menghapus database. Pesan "Invoke drop database" disalin ke variabel msg. Jika pengguna tidak memiliki izin akses, program mencetak pesan "No authorization upon database!" dan menyimpan pesan tersebut ke variabel msg.

Jika string query mengandung kata kunci "DROP TABLE", program akan melanjutkan ke blok kedua dalam if statement ini. Langkah-langkahnya mirip dengan yang dijelaskan sebelumnya. Program memecah string query menggunakan strtok() untuk mendapatkan nama tabel yang akan dihapus. Nama tabel disalin ke variabel dropTB. Program memanggil fungsi dropTable() untuk menghapus tabel dengan nama yang sesuai dari database yang sedang digunakan. Pesan "Invoke drop table" disalin ke variabel msg. Jika tidak ada database yang aktif, program mencetak pesan "Use a database first!" dan menyimpan pesan tersebut ke variabel msg.

Jika string query mengandung kata kunci "DROP COLUMN", program akan melanjutkan ke blok ketiga dalam if statement ini. Program memecah string query menggunakan strtok() untuk mendapatkan nama kolom yang akan dihapus serta nama tabel yang terkait. Nama kolom disalin ke variabel dropCol, sedangkan nama tabel disalin ke variabel sourceTB. Program mencetak pesan yang menunjukkan kolom dan tabel yang akan dihapus. Program memanggil fungsi removeColumn() untuk menghapus kolom tersebut dari tabel yang sesuai dalam database yang sedang digunakan. Pesan "Invoke drop column" disalin ke variabel msg. Jika tidak ada database yang aktif, program mencetak pesan "Use a database first!" dan menyimpan pesan tersebut ke variabel msg.
```
if(strstr(query, "DROP") != NULL){
            if(strstr(query, "DATABASE") != NULL){
                        char dropDB[1024];
                char *tokBuf = strtok(query, " ");
                tokBuf = strtok(NULL, " ");

                // Extract the nama database
                tokBuf = strtok(NULL, ";");
                strcpy(dropDB, tokBuf);
                if (tokBuf != NULL) {
                    printf("inserted drop database: %s\n", dropDB);
                }

                if(checkperm(user, dropDB) == 1){
                    dropdb(dropDB);
                    strcpy(msg, "Invoke drop database");  
                }else{
                    printf("%s has no right upon this %s database\n", user, dropDB);
                    strcpy(msg, "No authorization upon database!");
                }

            }
            else if(strstr(query, "TABLE") != NULL){
                if(strcmp(CURDATABASE, "") != 0){
                    char dropTB[1024];
                    char *tokBuf = strtok(query, " ");
                    tokBuf = strtok(NULL, " ");
                    tokBuf = strtok(NULL, ";");
                    strcpy(dropTB, tokBuf);
                    if (tokBuf != NULL) {
                        printf("inserted drop table: %s\n", dropTB);
                    }

                    dropTable(dropTB, CURDATABASE);
                    strcpy(msg, "Invoke drop table");       
                }else{
                    printf("Use a database first!\n");
                    strcpy(msg, "Use a databse first!");
                }

            }else if(strstr(query, "COLUMN") != NULL){
            if(strcmp(CURDATABASE, "") != 0){
                    char dropCol[1024], sourceTB[1024];
                    char *tokBuf = strtok(query, " ");
                    tokBuf = strtok(NULL, " ");

                    // Extract the nama database
                    tokBuf = strtok(NULL, " ");
                    strcpy(dropCol, tokBuf);
                    tokBuf = strtok(NULL, " ");
                    tokBuf = strtok(NULL, ";");
                    strcpy(sourceTB, tokBuf);

                    printf("inserted drop Column: %s from table: %s\n", dropCol, sourceTB);
                    removeColumn(sourceTB, dropCol);
                    strcpy(msg, "Invoke drop column");  
                }else{
                    printf("Use a database first!\n");
                    strcpy(msg, "Use a database first!");
                }

            }
          
        }
```
#### DROP DATABASE
```
int dropdb(char *database){
    char dirPath[2048];
    sprintf(dirPath, "%s/%s", dbpath, database);
    int status =  remove(dirPath);
    if (status == 0) {
        printf("Database removed: %s\n", database);
        return 1; // Database created successfully
    } else {
        perror("Failed to remove database");
        return 0; // Database creation failed
    }
}
```
Fungsi ini menerima satu parameter yaitu database, yang merupakan nama database yang akan dihapus. Tujuan dari fungsi ini adalah untuk menghapus direktori yang sesuai dengan nama database yang diberikan.

Pertama, kode ini menggunakan fungsi sprintf untuk menggabungkan dbpath (jalur direktori utama) dengan database (nama database) menjadi dirPath, yang akan menjadi jalur lengkap ke direktori database yang akan dihapus.

Selanjutnya, menggunakan fungsi remove, kode mencoba menghapus direktori yang disimpan dalam dirPath. Jika penghapusan berhasil, status akan bernilai 0. Pada baris ke-7, kondisi status == 0 diperiksa. Jika kondisi ini terpenuhi, maka penghapusan database dianggap berhasil. Pesan "Database removed: [nama database]" akan dicetak menggunakan printf, dan fungsi akan mengembalikan nilai 1 untuk menunjukkan keberhasilan penghapusan database.

Namun, jika kondisi status == 0 tidak terpenuhi, maka penghapusan database dianggap gagal. Pesan error akan dicetak menggunakan perror.
#### DROP TABLE
```
int dropTable(char *table, char *database){
    char filepath[1024];
    sprintf(filepath, "%s/%s/%s.csv", dbpath, database, table);
    printf("imminent remove table: %s\n", filepath);
    int status =remove(filepath);
    if (status == 0) {
        printf("Database removed: %s\n", database);
        return 1; // Database created successfully
    } else {
        perror("Failed to remove database");
        return 0; // Database creation failed
    }
    printf("Table '%s' removed successfully.\n", filepath);
}
```
Fungsi ini menerima dua parameter yaitu table (nama tabel yang akan dihapus) dan database (nama database yang mengandung tabel tersebut). Tujuan dari fungsi ini adalah untuk menghapus file CSV yang mewakili tabel dari database yang ditentukan.

Pertama, kode ini menggunakan fungsi sprintf untuk menggabungkan dbpath (jalur direktori utama), database (nama database), dan table (nama tabel) menjadi filepath, yang akan menjadi jalur lengkap ke file CSV yang akan dihapus.

Selanjutnya, pada baris ke-5, sebuah pesan "imminent remove table: [filepath]" akan dicetak menggunakan printf untuk menunjukkan file CSV yang akan dihapus.

Kemudian, menggunakan fungsi remove, kode mencoba menghapus file CSV yang disimpan dalam filepath. Jika penghapusan berhasil, status akan bernilai 0. Pada baris ke-7, kondisi status == 0 diperiksa. Jika kondisi ini terpenuhi, maka penghapusan tabel dianggap berhasil. Pesan "Table removed: [database]" akan dicetak menggunakan printf, dan fungsi akan mengembalikan nilai 1 untuk menunjukkan keberhasilan penghapusan tabel.

Namun, jika kondisi status == 0 tidak terpenuhi, maka penghapusan tabel dianggap gagal. Pesan error akan dicetak menggunakan perror.
#### DROP COLUMN
```
void removeColumn(char *filename, char *header) {
    char fname[2048];
    sprintf(fname, "%s/%s/%s.csv", dbpath, CURDATABASE, filename);

    FILE *file = fopen(fname, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char line[4004];
    char *columns[100];
    int columnCount = 0;
    int targetColumnIndex = -1;

    // Read the header line
    if (fgets(line, 4004, file) != NULL) {
        char *token = strtok(line, ",");
        while (token != NULL && columnCount < 100) {
            columns[columnCount] = token;
            if (strstr(token, header) != NULL) {
                targetColumnIndex = columnCount;
            }
            token = strtok(NULL, ",");
            columnCount++;
        }
    }

    if (targetColumnIndex == -1) {
        printf("Header not found in the file.\n");
        fclose(file);
        return;
    }

    // Create a temporary file to store modified data
    FILE *tempFile = tmpfile();
    if (tempFile == NULL) {
        printf("Failed to create a temporary file.\n");
        fclose(file);
        return;
    }

    // Write the modified header to the temporary file
    for (int i = 0; i < columnCount; i++) {
        if (i != targetColumnIndex) {
            fputs(columns[i], tempFile);
            if (i < columnCount - 1) {
                fputc(',', tempFile);
            }
        }
    }

    // Process the remaining lines
    while (fgets(line, 4004, file) != NULL) {
        char *token = strtok(line, ",");
        int columnIndex = 0;

        while (token != NULL && columnIndex < columnCount) {
            if (columnIndex != targetColumnIndex) {
                fputs(token, tempFile);
                if (columnIndex < columnCount - 1) {
                    fputc(',', tempFile);
                }
            }

            token = strtok(NULL, ",");
            columnIndex++;
        }
    }

    // Close the original file
    fclose(file);

    // Move the modified data from the temporary file to the original file
    rewind(tempFile);
    file = fopen(fname, "w");
    if (file == NULL) {
        printf("Failed to write to the file.\n");
        fclose(tempFile);
        return;
    }

    while (fgets(line, 4004, tempFile) != NULL) {
        fputs(line, file);
    }

    fclose(file);
    fclose(tempFile);

    printf("Column '%s' has been successfully removed from the file.\n", header);
}
```
Fungsi ini menerima dua parameter yaitu filename (nama file CSV) dan header (nama kolom yang akan dihapus). Tujuan dari fungsi ini adalah untuk membuka file CSV yang sesuai dengan filename dan menghapus kolom yang memiliki nama yang sesuai dengan header.

Pertama, kode ini menggunakan fungsi sprintf untuk menggabungkan dbpath (jalur direktori utama), CURDATABASE (nama database saat ini), dan filename (nama file CSV) menjadi fname, yang akan menjadi jalur lengkap menuju file CSV yang akan dimodifikasi.

Selanjutnya, kode mencoba membuka file CSV yang disimpan dalam fname menggunakan fungsi fopen.
Setelah file berhasil dibuka, fungsi akan membaca baris pertama file yang berisi header menggunakan fungsi fgets. Header tersebut akan dipisahkan menjadi token menggunakan fungsi strtok dengan delimiter berupa koma. Selama masih ada token dan jumlah kolom yang sudah terbaca masih kurang dari 100, nama kolom akan disimpan dalam array columns. Jika ditemukan kolom yang sesuai dengan header, indeks kolom tersebut akan disimpan dalam variabel targetColumnIndex.

Jika targetColumnIndex masih bernilai -1, artinya kolom yang sesuai dengan header tidak ditemukan dalam header file dan return eror;
Selanjutnya, fungsi akan membuat file sementara (temporary file) menggunakan fungsi tmpfile untuk menyimpan data yang sudah dimodifikasi.
Setelah berhasil membuat file sementara, fungsi akan menulis header yang sudah dimodifikasi ke dalam file sementara menggunakan fputs. Kolom yang memiliki indeks yang sama dengan targetColumnIndex akan diabaikan. Proses ini dilakukan dengan menggunakan perulangan for untuk menulis setiap kolom kecuali kolom yang akan dihapus. Setiap kolom dipisahkan dengan koma menggunakan fputc.
Selanjutnya, fungsi akan memproses baris-baris berikutnya dalam file. Setiap baris dibaca menggunakan fgets, kemudian kolom-kolomnya dipisahkan menjadi token menggunakan strtok. Setiap token akan ditulis ke dalam file sementara menggunakan fputs, dengan mengabaikan kolom yang memiliki indeks yang sama dengan targetColumnIndex. Seperti pada langkah sebelumnya, setiap kolom dipisahkan dengan koma menggunakan fputc.

Setelah selesai memproses semua baris, file asli ditutup menggunakan fclose. Kemudian, data yang sudah dimodifikasi dalam file sementara dipindahkan kembali ke file asli. Hal ini dilakukan dengan membuka file asli dalam mode "w" (write mode) menggunakan fopen, kemudian setiap baris dalam file sementara ditulis ke file asli menggunakan fputs.

Setelah seluruh data dipindahkan, kedua file (file asli dan file sementara) ditutup menggunakan fclose.
## DML
### INSERT INTO
```
if(strstr(query, "INSERT INTO") != NULL){
            if(strcmp(CURDATABASE, "") != 0){ 
                char tableName[1024], attr[1024];
                char *token = strtok(query, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, "(");
                strcpy(tableName, token);
                token = strtok(NULL, ")");
                strcpy(attr, token);
                insertTable(tableName, attr);
                strcpy(msg, "Invoke insert table");
            }
            else{
                printf("Use a database first to insert\n");
                strcpy(msg, "Use a databse first!");
            }
		}
```
Pada bagian ini (if(strstr(query, "INSERT INTO") != NULL)), program melakukan pengecekan apakah string query mengandung kata kunci "INSERT INTO" menggunakan fungsi strstr(). Jika kata kunci tersebut ditemukan, program akan melanjutkan ke dalam blok ini.
Di dalam blok tersebut, program melakukan pengecekan apakah variabel CURDATABASE tidak kosong (berarti ada database yang sedang digunakan). Hal ini dilakukan dengan membandingkan CURDATABASE dengan string kosong menggunakan fungsi strcmp(). Jika kondisi tersebut terpenuhi, program melanjutkan ke langkah selanjutnya. Jika tidak akan keluar dan return tidak using database.

Program kemudian mendeklarasikan dua variabel lokal, yaitu tableName dan attr, yang berfungsi untuk menyimpan nama tabel dan nilai-nilai atribut yang akan dimasukkan. Program menggunakan fungsi strtok() untuk memecah string query menjadi token-token yang dipisahkan oleh spasi. Token-token tersebut digunakan untuk mendapatkan nama tabel dan atribut-atribut dari string query. Nama tabel disalin ke variabel tableName, sedangkan atribut-atribut disalin ke variabel attr.

Selanjutnya, program memanggil fungsi insertTable() dengan parameter tableName dan attr untuk memasukkan data ke dalam tabel dengan nama dan atribut yang sesuai. Pesan "Invoke insert table" disalin ke variabel msg.

```
int insertTable(char *table, char *rawAttributes) {
    char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");

    sprintf(dirPath, "%s/%s/%s.csv", dbpath, CURDATABASE, tableBuf);
    printf("create table: %s\n", dirPath);

    FILE *tableFile = fopen(dirPath, "a");

    if(tableFile != NULL){
        printf("Table created: %s\n", table);
        char attrNames[1024];
        strcpy(attrNames, rawAttributes);
        fprintf(tableFile, "%s\n", attrNames);
        fclose(tableFile);
        return 1; // Database created successfully

    } else {
        perror("Failed to create table");
        return 0; // Database creation failed
    }
}
```
Fungsi ini menerima dua parameter yaitu table (nama tabel) dan rawAttributes (data atribut yang akan disisipkan). Tujuannya adalah untuk membuka file CSV yang sesuai dengan nama tabel yang diberikan, dan menuliskan data atribut ke dalam file tersebut.

Pertama, kode ini menggunakan fungsi strcpy untuk menyalin isi dari table ke dalam fixedTable. Kemudian, menggunakan fungsi strtok dengan delimiter spasi, tableBuf akan diisi dengan nama tabel yang telah diambil dari fixedTable. Hal ini dilakukan untuk memastikan bahwa hanya nama tabel yang digunakan dalam pembentukan dirPath nantinya.

Selanjutnya, menggunakan fungsi sprintf, dirPath akan diisi dengan jalur lengkap menuju file CSV yang akan dimodifikasi. Jalur ini terbentuk dari gabungan dbpath (jalur direktori utama), CURDATABASE (nama database saat ini), dan tableBuf (nama tabel).

Setelah itu, kode mencoba membuka file CSV yang disimpan dalam dirPath menggunakan fungsi fopen dengan mode "a" (append mode). Jika file berhasil dibuka, maka akan dicetak pesan "Table created: [nama tabel]" menggunakan printf, dan file akan ditutup menggunakan fclose. Selanjutnya, fungsi akan mengembalikan nilai 1 untuk menandakan bahwa penyisipan data ke dalam tabel berhasil.

Jika file gagal dibuka, pesan "Failed to create table" akan dicetak menggunakan perror, dan fungsi akan mengembalikan nilai 0 untuk menandakan bahwa penyisipan data gagal.
### UPDATE
```
```
Pada bagian ini (if(strstr(query, "UPDATE") != NULL && strstr(query, "WHERE") == NULL)), program melakukan pengecekan apakah string query mengandung kata kunci "UPDATE" dan tidak mengandung kata kunci "WHERE" menggunakan fungsi strstr(). Jika kondisi tersebut terpenuhi, program akan melanjutkan ke dalam blok ini.

Di dalam blok tersebut, program melakukan pengecekan apakah variabel CURDATABASE tidak kosong (berarti ada database yang sedang digunakan). Dilakukan seperti INSERT INTO karena memerlukan database yang digunakan

Program kemudian mendeklarasikan beberapa variabel lokal, seperti inpBuf, table, header, dan value, yang digunakan untuk menyimpan informasi yang dibutuhkan dalam operasi UPDATE. String query disalin ke variabel inpBuf untuk diproses. Program menggunakan fungsi strtok() untuk memecah string inpBuf menjadi token-token yang dipisahkan oleh spasi. Token-token tersebut digunakan untuk mendapatkan nama tabel, kolom, dan nilai yang akan diupdate. Nama tabel disalin ke variabel table, kolom disalin ke variabel header, dan nilai disalin ke variabel value.

Selanjutnya, program mencetak informasi mengenai data yang akan diupdate, yaitu nama tabel, kolom, dan nilai yang akan diubah. Program kemudian memanggil fungsi update() dengan parameter table, header, value, dan msg untuk melakukan operasi update pada basis data.

Pada bagian else if(strstr(query, "UPDATE") != NULL), program juga melakukan pengecekan pada string query untuk kata kunci "UPDATE". Hal ini dilakukan untuk kondis dimana ada WHERE dalam syntax.

Di dalam blok ini, program juga melakukan pengecekan apakah variabel CURDATABASE tidak kosong. Jika kondisi tersebut terpenuhi, program melanjutkan ke langkah selanjutnya.

Program mendeklarasikan beberapa variabel lokal yang serupa dengan blok sebelumnya, seperti inpBuf, table, header, value, wheader, dan wvalue. String query disalin ke variabel inpBuf untuk diproses. Program menggunakan fungsi strtok() untuk memecah string inpBuf menjadi token-token yang dipisahkan oleh spasi. Token-token tersebut digunakan untuk mendapatkan nama tabel, kolom, dan nilai yang akan diupdate, serta kolom dan nilai sebagai kondisi WHERE.

Selanjutnya, program mencetak informasi mengenai data yang akan diupdate, yaitu nama tabel, kolom, nilai yang akan diubah, serta kolom dan nilai sebagai kondisi WHERE. Program kemudian memanggil fungsi updateDatawhere() dengan parameter table, header, value, wheader, wvalue, dan msg untuk melakukan operasi update pada basis data dengan kondisi WHERE.
#### WITHOUT WHERE
```
int update(char *table, char *setColumn, char *setNewValue,char *response){
    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, table);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", fileName);
        return EXIT_FAILURE;
    }
    FILE *tempFile = fopen("temp.csv", "w");
    if (tempFile == NULL)
    {
        fclose(file);
        sprintf(response, "Failed to create the temporary file\n");
        return EXIT_FAILURE;
    }
    char line[2048];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)
    fputs(line,tempFile);
    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columncount = 0;
    while (token != NULL)
    {
        if (strstr(token, setColumn) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columncount++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }
    char dataLine[1024];
    int updated = 0;
    while (fgets(dataLine, sizeof(dataLine), file) != NULL)
    {
        // Remove any existing newline characters from the data line
        char *newline = strchr(dataLine, '\n');
        if (newline != NULL)
        {
            *newline = '\0';
        }

        // Extract the column values from the data line
        char *column = strtok(dataLine, ",");
        char currentColumn[100];
        int columnIndex = 0;

        while (column != NULL)
        {
            strcpy(currentColumn, column);

            if (columnIndex == columncount)
            {
                // Update the column value
                strcpy(currentColumn, setNewValue);
                updated = 1;
            }

            fprintf(tempFile, "%s", currentColumn);

            column = strtok(NULL, ",");
            columnIndex++;

            if (column != NULL)
            {
                fprintf(tempFile, ",");
            }
        }

        // Write a new line
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    if (updated)
    {
        // Replace the original file with the temporary file
        if (remove(fileName) != 0)
        {
            sprintf(response, "Failed to remove the original file\n");
            return EXIT_FAILURE;
        }

        if (rename("temp.csv", fileName) != 0)
        {
            sprintf(response, "Failed to rename the temporary file\n");
            return EXIT_FAILURE;
        }
        sprintf(response, "Data updated successfully.\n");
    }
    else
    {
        // Remove the temporary file if no updates were made
        remove("temp.csv");
        sprintf(response, "No matching records found for the given conditions.\n");
    }
    return EXIT_SUCCESS;
}
```
Fungsi update menerima empat parameter, yaitu table (nama tabel), setColumn (nama kolom yang akan diperbarui), setNewValue (nilai baru yang akan diberikan pada kolom tersebut), dan response (string untuk menyimpan respons pesan). Fungsi ini mengambil file CSV yang sesuai dengan nama tabel yang diberikan, dan mencari kolom yang sesuai dengan setColumn. Jika kolom tersebut ditemukan, fungsi akan mengubah nilai kolom dengan nilai baru yang diberikan dalam setNewValue. Setelah itu, fungsi akan menyimpan perubahan tersebut dalam file temporary (temp.csv). Jika ada perubahan yang dilakukan, file asli akan dihapus dan diganti dengan file temporary, sedangkan jika tidak ada perubahan, file temporary akan dihapus. Fungsi ini mengembalikan nilai EXIT_SUCCESS jika sukses, dan EXIT_FAILURE jika terjadi kegagalan.
#### WITH WHERE
```
int updateDatawhere(char *table, char *setColumn, char *setNewValue, char *whereColumn, char *whereValue, char *response)
{
  
    // Construct the file path
    char filePath[2048];
    sprintf(filePath, "databases/%s/%s.csv", CURDATABASE, table);

    // Open the CSV file for reading
    FILE *file = fopen(filePath, "r");
    if (file == NULL)
    {
        sprintf(response, "Failed to open the file '%s'\n", filePath);
        return EXIT_FAILURE;
    }

    // Create a temporary file for writing the updated data
    FILE *tempFile = fopen("temp.csv", "w");
    if (tempFile == NULL)
    {
        fclose(file);
        sprintf(response, "Failed to create the temporary file\n");
        return EXIT_FAILURE;
    }

    // Read and process the header line
    char headerLine[1024];
    fgets(headerLine, sizeof(headerLine), file);
    fputs(headerLine, tempFile);

    // Read and process each data line
    char dataLine[1024];
    int updated = 0;

    while (fgets(dataLine, sizeof(dataLine), file) != NULL)
    {
        // Remove any existing newline characters from the data line
        char *newline = strchr(dataLine, '\n');
        if (newline != NULL)
        {
            *newline = '\0';
        }

        // Extract the column values from the data line
        char *column = strtok(dataLine, ",");
        char currentColumn[100];
        int columnIndex = 0;

        while (column != NULL)
        {
            strcpy(currentColumn, column);

            if (strcmp(currentColumn, whereValue) == 0 && strcmp(setColumn, whereColumn) == 0)
            {
                // Update the column value
                strcpy(currentColumn, setNewValue);
                updated = 1;
            }

            fprintf(tempFile, "%s", currentColumn);

            column = strtok(NULL, ",");
            columnIndex++;

            if (column != NULL)
            {
                fprintf(tempFile, ",");
            }
        }

        // Write a new line
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    if (updated)
    {
        // Replace the original file with the temporary file
        if (remove(filePath) != 0)
        {
            sprintf(response, "Failed to remove the original file\n");
            return EXIT_FAILURE;
        }

        if (rename("temp.csv", filePath) != 0)
        {
            sprintf(response, "Failed to rename the temporary file\n");
            return EXIT_FAILURE;
        }
        sprintf(response, "Data updated successfully.\n");
    }
    else
    {
        // Remove the temporary file if no updates were made
        remove("temp.csv");
        sprintf(response, "No matching records found for the given conditions.\n");
    }
    return EXIT_SUCCESS;
}
```
Fungsi updateDatawhere mirip dengan fungsi update, namun memiliki tambahan dua parameter yaitu whereColumn (kolom untuk kondisi WHERE) dan whereValue (nilai untuk kondisi WHERE). Fungsi ini melakukan pembaruan data pada kolom yang sesuai dengan kondisi WHERE yang ditentukan. Jika ada perubahan yang dilakukan, file asli akan dihapus dan diganti dengan file temporary, sedangkan jika tidak ada perubahan, file temporary akan dihapus. Fungsi ini juga mengembalikan nilai EXIT_SUCCESS jika sukses, dan EXIT_FAILURE jika terjadi kegagalan.
### DELETE FROM
```
if(strstr(query, "DELETE FROM") != NULL && strstr(query, "WHERE") == NULL){
            if(strcmp(CURDATABASE, "") != 0){
                char tableName[1024];
                char *token = strtok(query, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, ";");
                strcpy(tableName, token);
                deletedata(tableName);
                strcpy(msg, "Invoke delete table data");
            }else{
                printf("Use a database first to delete");
                strcpy(msg, "Use a database first!");
            }
		}else if(strstr(query, "DELETE FROM") != NULL){
	    	if(strcmp(CURDATABASE, "") != 0){
	        char inpBuf[1024], table[1024], column[1024], value[1024];
                strcpy(inpBuf, query);
                 char *token = strtok(inpBuf, " ");
                 token = strtok(NULL, " ");
                 token = strtok(NULL, " ");
                 strcpy(table, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, "=");
                    strcpy(column, token);
                    token = strtok(NULL, ";");
                    strcpy(value, token);
                    printf("Update data from : %s\nkolom : %s\nvalue : %s\n", table, column, value);
                    delete_from_table_where(table,column,value);
                    strcpy(msg, "Invoke delete table data");
	        }
```
ada bagian ini (if(strstr(query, "DELETE FROM") != NULL && strstr(query, "WHERE") == NULL)), program melakukan pengecekan apakah string query mengandung kata kunci "DELETE FROM" dan tidak mengandung kata kunci "WHERE" menggunakan fungsi strstr(). Jika kondisi tersebut terpenuhi, program akan melanjutkan ke dalam blok ini.

Di dalam blok tersebut, program melakukan pengecekan apakah variabel CURDATABASE tidak kosong (berarti ada database yang sedang digunakan). Alasan sama seperti update dan insert into yang memerlukan database.

Program kemudian mendeklarasikan variabel lokal tableName untuk menyimpan nama tabel yang akan dihapus. String query diproses menggunakan fungsi strtok() untuk memecah string menjadi token-token yang dipisahkan oleh spasi. Token-token tersebut digunakan untuk mendapatkan nama tabel yang akan dihapus. Nama tabel disalin ke variabel tableName.

Selanjutnya, program memanggil fungsi deletedata(tableName) dengan parameter tableName untuk menghapus seluruh data dalam tabel tersebut. Program juga mengupdate string msg dengan pesan yang sesuai.

Pada bagian else if(strstr(query, "DELETE FROM") != NULL), program juga melakukan pengecekan pada string query untuk kata kunci "DELETE FROM". Jika kondisi tersebut terpenuhi, program melanjutkan ke dalam blok ini.

Program mendeklarasikan beberapa variabel lokal yang serupa dengan blok sebelumnya, seperti inpBuf, table, column, dan value. String query disalin ke variabel inpBuf untuk diproses. Program menggunakan fungsi strtok() untuk memecah string inpBuf menjadi token-token yang dipisahkan oleh spasi. Token-token tersebut digunakan untuk mendapatkan nama tabel, kolom, dan nilai yang akan dihapus.

Selanjutnya, program mencetak informasi mengenai data yang akan dihapus, yaitu nama tabel, kolom, dan nilai yang akan dihapus. Program kemudian memanggil fungsi delete_from_table_where() dengan parameter table, column, value untuk menghapus data dari tabel dengan kondisi WHERE yang diberikan.
#### WITHOUT WHERE
```
void deletedata(const char* filename) {
    char fname[2048];
    sprintf(fname, "%s/%s/%s.csv", dbpath, CURDATABASE, filename);
    char tempf[2048];
     sprintf(tempf, "%s/%s/temp.csv", dbpath, CURDATABASE);
    FILE* file = fopen(fname, "r");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    char buffer[1024];
    fgets(buffer, sizeof(buffer), file); // Read the first row

    FILE* temp = fopen(tempf, "w");
    fprintf(temp, "%s", buffer); // Write the first row to a temporary file

    fclose(temp);
    fclose(file);

    remove(fname); // Delete the original file
    rename(tempf, fname); // Rename the temporary file to the original file name

    printf("Rows deleted successfully except for the first row.\n");
}
```
Fungsi deletedata menerima parameter filename yang merupakan nama file CSV yang akan dihapus. Fungsi ini membuka file CSV yang sesuai dengan path yang dibangun menggunakan dbpath, CURDATABASE, dan filename. Jika file berhasil dibuka, fungsi ini membaca baris pertama dari file (header) dan menyalinnya ke file temporary. Setelah itu, file asli dihapus menggunakan fungsi remove, dan file temporary diubah namanya menjadi nama file asli menggunakan fungsi rename. Fungsi ini menghasilkan pesan bahwa baris telah dihapus kecuali baris pertama (header).
#### WITH WHERE
```
int delete_from_table_where(char *table, char *columnName, char *condition)
{
    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, table);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char tempFileName[2048 + 100];
    sprintf(tempFileName, "databases/%s/temp.csv", CURDATABASE);

    FILE *tempFile = fopen(tempFileName, "w");
    if (tempFile == NULL)
    {
        printf("Failed to create temporary CSV file.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    char line[2048];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)
    fputs(line, tempFile);
    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        fclose(file);
        fclose(tempFile);
        return EXIT_FAILURE;
    }
    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[2048];
        strcpy(tempLine, line);

        // Extract the column values from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;
        int matchFound = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                if (strcmp(columnValue, condition) == 0) // Check if the condition is met
                {
                    matchFound = 1;
                    break;
                }
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }

        // If the condition is not met, write the line to the temporary file
        if (!matchFound)
            fputs(line, tempFile);
    }

    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    remove(fileName);
    rename(tempFileName, fileName);

    return 1;
}
```
Fungsi delete_from_table_where menerima tiga parameter, yaitu table (nama tabel), columnName (nama kolom untuk kondisi WHERE), dan condition (nilai yang harus memenuhi kondisi WHERE). Fungsi ini membuka file CSV yang sesuai dengan nama tabel yang diberikan, dan mencari kolom yang sesuai dengan columnName. Jika kolom tersebut ditemukan, fungsi akan membandingkan nilai pada kolom dengan kondisi yang diberikan dalam condition. Jika kondisi terpenuhi, baris tersebut akan diabaikan dan tidak ditulis ke file temporary. Jika kondisi tidak terpenuhi, baris tersebut akan ditulis ke file temporary. Setelah proses pembacaan selesai, file asli dihapus menggunakan fungsi remove, dan file temporary diubah namanya menjadi nama file asli menggunakan fungsi rename. Fungsi ini mengembalikan nilai 1 jika sukses.
### SELECT
```
if(strstr(query, "SELECT") != NULL){
	    	if(strcmp(CURDATABASE, "") != 0){
                char inpBuf[1024], table[1024], column[1024], value[1024];
                if(strstr(query, "*") != NULL){
                strcpy(inpBuf, query);
                 char *token = strtok(inpBuf, " ");
                 token = strtok(NULL, " ");
                 token = strtok(NULL, " ");
                 token = strtok(NULL, ";");
                 strcpy(table, token);
                 selectAll(table,msg);
                } else if(strstr(query, "WHERE") != NULL){
                strcpy(inpBuf, query);
                 char *token = strtok(inpBuf, " ");
                 token = strtok(NULL, " ");
                 strcpy(column, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, " ");
                    strcpy(table, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, "=");
                    token = strtok(NULL, ";");
                    strcpy(value, token);
                    printf("Update data from : %s\nkolom : %s\nvalue : %s\n", table, column, value);
                    select_column_from_table_where(column,table,value,msg);
                }else {
                    strcpy(inpBuf, query);
                    char *token = strtok(inpBuf, " ");
                    token = strtok(NULL, " ");
                    strcpy(column, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, ";");
                    strcpy(table, token);
                    select_column_from_table(column,table,msg);
                }
	        }
```
Pada bagian ini (if(strstr(query, "SELECT") != NULL)), program melakukan pengecekan apakah string query mengandung kata kunci "SELECT" menggunakan fungsi strstr().

Program mendeklarasikan beberapa variabel lokal, seperti inpBuf, table, column, dan value. Variabel inpBuf digunakan untuk menyimpan salinan dari string query yang akan diproses. Selanjutnya, program melakukan serangkaian pengecekan pada string query untuk menentukan tipe operasi SELECT yang akan dilakukan.

Jika string query mengandung karakter "*", program menganggapnya sebagai SELECT * (memilih semua kolom) dan melanjutkan dengan mendapatkan nama tabel yang dituju dari string query. Program memanggil fungsi selectAll(table, msg) untuk memilih semua data dari tabel tersebut.

Jika string query mengandung kata kunci "WHERE", program menganggapnya sebagai SELECT dengan kondisi WHERE. Program mendapatkan nama kolom, nama tabel, dan nilai yang digunakan sebagai kondisi WHERE dari string query. Program memanggil fungsi select_column_from_table_where(column, table, value, msg) untuk memilih data dari tabel dengan kondisi WHERE yang diberikan.

Jika tidak terdapat karakter "*" maupun kata kunci "WHERE" dalam string query, program menganggapnya sebagai SELECT dengan kolom tertentu. Program mendapatkan nama kolom dan nama tabel dari string query dan memanggil fungsi select_column_from_table(column, table, msg) untuk memilih data dari tabel dengan kolom yang ditentukan.
#### SELECT *
```
int selectAll(char *table, char *response)
{

    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, table);

    // Open the file for reading
    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", fileName);
        return EXIT_FAILURE;
    }

    // Read and print each line in the file
    char line[2048];
    strcat(response, "\n");
    while (fgets(line, sizeof(line), file) != NULL)
    {
        strcat(response, line);
    }

    // Close the file
    fclose(file);

    return EXIT_SUCCESS;
}

```
Fungsi selectAll menerima dua parameter, yaitu table (nama tabel) dan response (variabel penampung hasil). Fungsi ini membuka file CSV yang sesuai dengan nama tabel yang diberikan dan membaca setiap baris dalam file tersebut. Setiap baris yang dibaca kemudian ditambahkan ke variabel response. Setelah proses pembacaan selesai, file ditutup. Jika file tidak dapat dibuka, fungsi ini mengisi response dengan pesan bahwa tabel yang dimaksud tidak ada dan mengembalikan nilai EXIT_FAILURE. Jika semua proses berjalan dengan sukses, fungsi ini mengembalikan nilai EXIT_SUCCESS
#### SELECT WHERE
```

int select_column_from_table(char *columnName, char *tableName, char *response)
{
    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", fileName);
        return EXIT_FAILURE;
    }

    char line[2048];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[2048];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Append the column value to the response
                strcat(response, columnValue);
                strcat(response, "\n");
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}

```
Fungsi select_column_from_table mirip dengan selectAll, namun hanya kolom yang ditentukan yang akan ditampilkan. Fungsi ini menerima tiga parameter, yaitu columnName (nama kolom yang akan ditampilkan), tableName (nama tabel), dan response (variabel penampung hasil). Fungsi ini membuka file CSV yang sesuai dengan nama tabel yang diberikan dan mencari kolom yang sesuai dengan columnName. Jika kolom tersebut ditemukan, fungsi akan menyalin nilai pada kolom tersebut ke variabel response. Setelah itu, setiap baris dalam file dibaca, dan nilai dari kolom yang sesuai ditambahkan ke response. Jika file tidak dapat dibuka atau kolom yang diminta tidak ada, fungsi ini mengisi response dengan pesan yang sesuai dan mengembalikan nilai EXIT_FAILURE. Jika semua proses berjalan dengan sukses, fungsi ini mengembalikan nilai EXIT_SUCCESS.
#### NORMAL
```

int select_column_from_table_where(char *columnName, char *tableName, char *condition, char *response)
{

    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", fileName);
        return EXIT_FAILURE;
    }

    char line[2048];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[2048];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Check if the condition is met
                if (strcmp(columnValue, condition) == 0)
                {
                    // Append the column value to the response
                    strcat(response, columnValue);
                    strcat(response, "\n");
                }
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return 0;
}
```
Fungsi select_column_from_table_where adalah perluasan dari select_column_from_table dengan tambahan fitur kondisi WHERE. Fungsi ini menerima empat parameter, yaitu columnName (nama kolom yang akan ditampilkan), tableName (nama tabel), condition (kondisi yang harus dipenuhi), dan response (variabel penampung hasil). Fungsi ini memiliki proses yang serupa dengan select_column_from_table, namun hanya baris yang memenuhi kondisi yang akan ditambahkan ke response. Jika file tidak dapat dibuka, kolom yang diminta tidak ada, atau tidak ada baris yang memenuhi kondisi, fungsi ini mengisi response dengan pesan yang sesuai dan mengembalikan nilai EXIT_FAILURE. Jika semua proses berjalan dengan sukses, fungsi ini mengembalikan nilai EXIT_SUCCESS.
## LOGGING
Kita akan melakukan lgging setiap kali perintah dijalankan, akan ada fungsi yang akan menulis semua yang dilakukan client dalam sebuah file bernama log.txt dan menggunakan fungsi update log.
```
void updatelog(char *user, char *command){
    
    time_t currentTime = time(NULL);
    struct tm *localTime = localtime(&currentTime);

    // Format the current time as a string
    char timeString[120];
    strftime(timeString, sizeof(timeString), "%Y-%m-%d %H:%M:%S", localTime);

    FILE *log = fopen(logfile, "a");
    if (log == NULL) {
        printf("Error opening file.\n");
        return;
    }
        fprintf(log, "%s:%s:%s\n", timeString, user, command);
        fclose(log);

}
```
fungsi updatelog yang bertujuan untuk mencatat log penggunaan aplikasi atau sistem.

Pertama, kode ini menggunakan time untuk mendapatkan waktu saat ini dalam bentuk Unix time (jumlah detik sejak 1 Januari 1970). Selanjutnya, menggunakan localtime, waktu tersebut dikonversi menjadi struktur struct tm yang berisi informasi tentang waktu lokal, seperti tahun, bulan, tanggal, jam, dan menit.

Kemudian, menggunakan fungsi strftime, kode memformat waktu yang telah diperoleh menjadi string dengan format "YYYY-MM-DD HH:MM:SS" dan menyimpannya dalam array karakter timeString dengan ukuran 120.

Selanjutnya, kode mencoba membuka file log menggunakan fopen dengan mode "a" (append), yang berarti log akan ditambahkan ke akhir file jika file sudah ada atau akan dibuat jika file belum ada. Jika pembukaan file gagal, maka kode akan mencetak pesan error menggunakan printf dan fungsi updatelog akan dihentikan dengan pernyataan return.

Jika pembukaan file log berhasil, kode menggunakan fprintf untuk menulis log ke dalam file. Log ini terdiri dari waktu (timeString), nama pengguna (user), dan perintah (command). Setelah penulisan log selesai, kode menutup file menggunakan fclose.
```
while(1){ 
    -----------------------
            updatelog(user,buffer);
        send(new_socket, msg, strlen(msg), 0);
        fflush(stdout); 
}
```
## DUMP BACKUP
Pertama, program akan memeriksa jumlah argumen baris perintah yang diberikan. Jika jumlah argumen tidak sama dengan 5, program akan mencetak pesan kesalahan dan keluar dengan status keluaran yang menunjukkan kegagalan.

Selanjutnya, program akan memeriksa apakah argumen yang diberikan sesuai dengan format yang diharapkan. Argumen pertama harus berupa "-u", argumen ketiga harus berupa "-p", dan argumen kelima adalah nama database. Jika argumen tidak sesuai format, program akan mencetak pesan kesalahan dan keluar dengan status keluaran yang menunjukkan kegagalan.
Setelah validasi argumen, program akan memeriksa apakah pengguna memiliki izin akses. Jika program dijalankan dengan hak akses "root", pengguna dianggap memiliki izin akses. Jika tidak, program akan memanggil fungsi cekperm untuk memeriksa izin akses pengguna terhadap database. Fungsi cekperm membaca file "perm.csv" yang berisi daftar pengguna dan database yang diizinkan. Jika pengguna memiliki akses, fungsi ini mengembalikan nilai 1.

Jika pengguna tidak memiliki izin akses, program akan berhenti. Jika pengguna memiliki izin akses, program akan melanjutkan dengan membuka file "log.txt" dan membuat file "command.backup.txt". Program akan membaca isi file "log.txt" dan menyalin kontennya ke file "command.backup.txt". Bagian ini dilakukan dengan membaca baris per baris dari "log.txt", memotong string setelah tanda titik dua (":") ke file "command.backup.txt". Hal ini akan memastikan isi dari backup hanya berisi command command saja tanpa waktu dan nama yang ada di "log.txt", sehingga sesuai dengan permintaan tugas
Terakhir, program akan menutup file yang telah dibuka dan keluar dengan status keluaran yang menunjukkan keberhasilan.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

struct allowed {
    char name[10000];
    char password[10000];
};

int cekperm(char *usr, char *db) {
   if (strcmp(usr, "Root") == 0)
        return 1; // Root has access to all databases

    FILE *file = fopen("../database/perm.csv", "r");
    if (file != NULL) {
        char line[256];
        char curusr[1024];
        char curdb[1024];

        while (fgets(line, sizeof(line), file)) {
            sscanf(line, "%[^,],%s", curusr, curdb);

            if (strcmp(curusr, usr) == 0 && strcmp(curdb, db) == 0) {
                fclose(file);
                return 1; // User has access rights to the database
            }
        }

        fclose(file);
    }

}
int main(int argc, char *argv[]){
 if (argc != 5) {
        printf("Usage: %s -u [username] -p [password] [database]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printf("Usage: %s -u [username] -p [password] [database]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    int allowed = 0;
    if (geteuid() == 0) {
        allowed = 1;
    } else {
        allowed = cekperm(argv[2], argv[5]);
    }
    if (allowed == 0) {
        return 0;
    }
    int ch;
    FILE *fp, *dp;
    fp = fopen("../database/log.txt", "r");
    dp = fopen("command.backup.txt", "w");
    if (fp == NULL) {
        printf("Failed to open log file.\n");
        return 0;
    }
    char buffer[20000];
    while (fgets(buffer, sizeof(buffer), fp)) {
        char *colon = strchr(buffer, ':');
        if (colon != NULL) {
            // Move the pointer forward to skip timestamp and username
            colon = strchr(colon + 1, ':');
            colon = strchr(colon + 1, ':');
            colon = strchr(colon + 1, ':');
            if (colon != NULL) {
                fprintf(dp, "%s", colon + 1);
            }
        }
    }
    fclose(fp);
    fclose(dp);
    return 0;
}


```
## Penjelasan Docker
Dalam Final Praktikum ini terdapat docker yang harus dibuat dan dijalankan. Terdapat 3 point yang harus berhasil di buat, yaitu :
### A. Docker File
Pertama-tama kita akan membuat docker file, dengan code berikut :

FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    build-essential \
    gcc \
    libc6-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY ./database/database.c .
COPY ./client/client.c .

RUN gcc -o database database.c
RUN gcc -o client client.c

COPY run.sh .
RUN chmod +x run.sh

CMD ["./run.sh"]


Pertama menentukan docker images yang dibuat yaitu menggunakan ubuntu dengan versi latest.

Lalu menginstall beberapa library yang dibutuhkan dari image ubuntu kita dengan run apt-get update dll.

Lalu set WORKDIR nya di dalam directory /app

Lalu mengcopy isi dari database.c dan client.c yang terdapat di directory awal di ubuntu kita /database dan /client kedalam directory default dari image ubuntu dengan "."

Lalu mengcompile database. dan client.c dengan RUN gcc ...

Lalu mengcopy run.sh yang berisi tahapan pendukung dari dockerfile ini berjalan yang berisi code :

#!/bin/bash
./database &
sleep 5
./client -u informatika -p informatika123


Dimana fungsi dari run.sh ini untuk memberika jeda selama 5 detik dari database dan client untuk di jalankan terlebih dahulu oleh dockerfile ini.

Lalu setelah run.sh ini, terdapan chmod +x run.sh yang berfungsi memberika access supaya run.sh ini bisa dijalankan.

Lalu menjalankan run.sh


### B. Docker Hub
Isi dari dockerhub.txt ini adalah :

https://hub.docker.com/r/gungadhisanjaya/storage-app

Dengan cara kita "docker login" terlebih dahulu, lalu kita melakukan "docker push gungadhisanjaya/storage-app" agar isi dari Dockerfile bisa push ke dalam repository account docker kita.

### C. Docker Compose

version: '3'

services:
  sukolilo:
    build:
      context: ./Sukolilo
      dockerfile: Dockerfile
    deploy:
      replicas: 5

  keputih:
    build:
      context: ./Keputih
      dockerfile: Dockerfile
    deploy:
      replicas: 5

  gebang:
    build:
      context: ./Gebang
      dockerfile: Dockerfile
    deploy:
      replicas: 5
     
  mulyos:
    build:
      context: ./Mulyos
      dockerfile: Dockerfile
    deploy:
      replicas: 5
      
  semolowaru:
    build:
      context: ./Semolowaru
      dockerfile: Dockerfile
    deploy:
      replicas: 5

Jadi kita akan membuat docker compose ini dengan instance sebanyak 5, dimana code diatas pertama-tama kita membuat versionnya menjadi "3" lalu didalam services nya berisi nama dari tiap folder yang kita buat lalu contextnya ./namafolder dimana berisi nama dari folder yg sudah di tetapkan dengan replicas 5 untuk instance sebanyak 5. Di buat sebanyak 5 folder yang sudah di tentukan dan membuat folder tersebut juga di dalam curent directory dengan berisi file file yang harus di jalankan seperti client, database dll.