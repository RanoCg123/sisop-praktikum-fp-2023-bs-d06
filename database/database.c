#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <ctype.h>
#define dbpath "./databases"
#define usrfile "users.txt"
#define logfile "log.txt"
#define permfile "perm.csv"
#define PORT 8080
char CURDATABASE[1024]="";
void makedbdir () {
    const char* directoryName = "databases";

    int status = mkdir(directoryName, 0777);  // Create the directory

    if (status == 0) {
        printf("Directory created successfully.\n");
    } else {
        printf("Failed to create the directory.\n");
    }
}
void mkusers(){
    FILE *userTableFile = fopen(usrfile, "a");
    if (userTableFile == NULL) {
        perror("Failed to create user table");
        exit(EXIT_FAILURE);
    }
    FILE *permTableFile = fopen(permfile, "a");
    if (permTableFile == NULL) {
        perror("Failed to create user table");
        exit(EXIT_FAILURE);
    }
}
void updatelog(char *user, char *command){
    
    time_t currentTime = time(NULL);
    struct tm *localTime = localtime(&currentTime);

    // Format the current time as a string
    char timeString[120];
    strftime(timeString, sizeof(timeString), "%Y-%m-%d %H:%M:%S", localTime);

    FILE *log = fopen(logfile, "a");
    if (log == NULL) {
        printf("Error opening file.\n");
        return;
    }
        fprintf(log, "%s:%s:%s\n", timeString, user, command);
        fclose(log);

}

int checkusr(const char* usr) {
    FILE* file = fopen(usrfile, "r");
    if (file == NULL) {
        printf("File %s could not be opened.\n", usrfile);
        return 0;
    }

    char line[256];
    while (fgets(line, sizeof(line), file) != NULL) {
        if (strncmp(line, usr, strlen(usr)) == 0) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}

void crusr(const char* usr, const char* pswd) {
    FILE* file = fopen(usrfile, "a");
    if (file == NULL) {
        printf("File %s could not be opened.\n", usrfile);
        return;
    }

    fprintf(file, "%s %s\n", usr, pswd);
    fclose(file);
}
int mkperm(char *usr, char *db) {
    FILE *file = fopen(permfile, "a");
    if (file == NULL) {
        printf("File could not be opened.\n");
        return 0;
    }
        fprintf(file, "%s,%s\n", usr, db);
        fclose(file);
        return 1;
}
int checkperm(char *usr, char *db){
    if (strcmp(usr, "Root") == 0)
        return 1; // Root has access to all databases

    FILE *file = fopen(permfile, "r");
    if (file != NULL) {
        char line[256];
        char curusr[1024];
        char curdb[1024];

        while (fgets(line, sizeof(line), file)) {
            sscanf(line, "%[^,],%s", curusr, curdb);

            if (strcmp(curusr, usr) == 0 && strcmp(curdb, db) == 0) {
                fclose(file);
                return 1; // User has access rights to the database
            }
        }

        fclose(file);
    }

    return 0; // User does not have access rights to the database
}
int mkdb(char *database) {
    char dirPath[100];
    sprintf(dirPath, "%s/%s", dbpath, database);

    int status = mkdir(dirPath, 0777);
    if (status == 0) {
        printf("Database created: %s\n", database);
        return 1; // Database created successfully
    } else {
        perror("Failed to create database");
        return 0; // Database creation failed
    }
}
int dropdb(char *database){
    char dirPath[2048];
    sprintf(dirPath, "%s/%s", dbpath, database);
    int status =  remove(dirPath);
    if (status == 0) {
        printf("Database removed: %s\n", database);
        return 1; // Database created successfully
    } else {
        perror("Failed to remove database");
        return 0; // Database creation failed
    }
}

void extractAttributeNames(const char* inputString, char* outputString) {
    char attrBuf[1024];
    strcpy(attrBuf, inputString);

    char* token;
    char* savePtr;
    char* attributeName;

    token = strtok_r(attrBuf, ",", &savePtr);
    while (token != NULL) {
        // Find the first space character to separate attribute name
        attributeName = strtok(token, " ");
        if (attributeName != NULL) {
            strcat(outputString, attributeName);
            strcat(outputString, ",");
        }
        token = strtok_r(NULL, ",", &savePtr);
    }

    // Remove the trailing comma, if any
    if (strlen(outputString) > 0) {
        outputString[strlen(outputString) - 1] = '\0';
    }
    printf("Invoke extract attrnames, results: %s\n", outputString);
}

int createTable(char *table, char *rawAttributes) {
    char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");

    sprintf(dirPath, "%s/%s/%s.csv", dbpath, CURDATABASE, tableBuf);
    printf("create table: %s\n", dirPath);

    FILE *tableFile = fopen(dirPath, "a");

    if(tableFile != NULL){
        printf("Table created: %s\n", table);
        char attrNames[1024];
        extractAttributeNames(rawAttributes, attrNames);

        fprintf(tableFile, "%s\n", attrNames);
        fclose(tableFile);
        return 1; // Database created successfully

    } else {
        perror("Failed to create table");
        return 0; // Database creation failed
    }
}
int insertTable(char *table, char *rawAttributes) {
    char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");

    sprintf(dirPath, "%s/%s/%s.csv", dbpath, CURDATABASE, tableBuf);
    printf("create table: %s\n", dirPath);

    FILE *tableFile = fopen(dirPath, "a");

    if(tableFile != NULL){
        printf("Table created: %s\n", table);
        char attrNames[1024];
        strcpy(attrNames, rawAttributes);
        fprintf(tableFile, "%s\n", attrNames);
        fclose(tableFile);
        return 1; // Database created successfully

    } else {
        perror("Failed to create table");
        return 0; // Database creation failed
    }
}
int dropTable(char *table, char *database){
    char filepath[1024];
    sprintf(filepath, "%s/%s/%s.csv", dbpath, database, table);
    printf("imminent remove table: %s\n", filepath);
    int status =remove(filepath);
    if (status == 0) {
        printf("Database removed: %s\n", database);
        return 1; // Database created successfully
    } else {
        perror("Failed to remove database");
        return 0; // Database creation failed
    }
    printf("Table '%s' removed successfully.\n", filepath);
}
void removeColumn(char *filename, char *header) {
    char fname[2048];
    sprintf(fname, "%s/%s/%s.csv", dbpath, CURDATABASE, filename);

    FILE *file = fopen(fname, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char line[4004];
    char *columns[100];
    int columnCount = 0;
    int targetColumnIndex = -1;

    // Read the header line
    if (fgets(line, 4004, file) != NULL) {
        char *token = strtok(line, ",");
        while (token != NULL && columnCount < 100) {
            columns[columnCount] = token;
            if (strstr(token, header) != NULL) {
                targetColumnIndex = columnCount;
            }
            token = strtok(NULL, ",");
            columnCount++;
        }
    }

    if (targetColumnIndex == -1) {
        printf("Header not found in the file.\n");
        fclose(file);
        return;
    }

    // Create a temporary file to store modified data
    FILE *tempFile = tmpfile();
    if (tempFile == NULL) {
        printf("Failed to create a temporary file.\n");
        fclose(file);
        return;
    }

    // Write the modified header to the temporary file
    for (int i = 0; i < columnCount; i++) {
        if (i != targetColumnIndex) {
            fputs(columns[i], tempFile);
            if (i < columnCount - 1) {
                fputc(',', tempFile);
            }
        }
    }

    // Process the remaining lines
    while (fgets(line, 4004, file) != NULL) {
        char *token = strtok(line, ",");
        int columnIndex = 0;

        while (token != NULL && columnIndex < columnCount) {
            if (columnIndex != targetColumnIndex) {
                fputs(token, tempFile);
                if (columnIndex < columnCount - 1) {
                    fputc(',', tempFile);
                }
            }

            token = strtok(NULL, ",");
            columnIndex++;
        }
    }

    // Close the original file
    fclose(file);

    // Move the modified data from the temporary file to the original file
    rewind(tempFile);
    file = fopen(fname, "w");
    if (file == NULL) {
        printf("Failed to write to the file.\n");
        fclose(tempFile);
        return;
    }

    while (fgets(line, 4004, tempFile) != NULL) {
        fputs(line, file);
    }

    fclose(file);
    fclose(tempFile);

    printf("Column '%s' has been successfully removed from the file.\n", header);
}
void deletedata(const char* filename) {
    char fname[2048];
    sprintf(fname, "%s/%s/%s.csv", dbpath, CURDATABASE, filename);
    char tempf[2048];
     sprintf(tempf, "%s/%s/temp.csv", dbpath, CURDATABASE);
    FILE* file = fopen(fname, "r");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    char buffer[1024];
    fgets(buffer, sizeof(buffer), file); // Read the first row

    FILE* temp = fopen(tempf, "w");
    fprintf(temp, "%s", buffer); // Write the first row to a temporary file

    fclose(temp);
    fclose(file);

    remove(fname); // Delete the original file
    rename(tempf, fname); // Rename the temporary file to the original file name

    printf("Rows deleted successfully except for the first row.\n");
}
int delete_from_table_where(char *table, char *columnName, char *condition)
{
    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, table);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char tempFileName[2048 + 100];
    sprintf(tempFileName, "databases/%s/temp.csv", CURDATABASE);

    FILE *tempFile = fopen(tempFileName, "w");
    if (tempFile == NULL)
    {
        printf("Failed to create temporary CSV file.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    char line[2048];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)
    fputs(line, tempFile);
    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        fclose(file);
        fclose(tempFile);
        return EXIT_FAILURE;
    }
    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[2048];
        strcpy(tempLine, line);

        // Extract the column values from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;
        int matchFound = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                if (strcmp(columnValue, condition) == 0) // Check if the condition is met
                {
                    matchFound = 1;
                    break;
                }
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }

        // If the condition is not met, write the line to the temporary file
        if (!matchFound)
            fputs(line, tempFile);
    }

    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    remove(fileName);
    rename(tempFileName, fileName);

    return 1;
}
int selectAll(char *table, char *response)
{

    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, table);

    // Open the file for reading
    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", fileName);
        return EXIT_FAILURE;
    }

    // Read and print each line in the file
    char line[2048];
    strcat(response, "\n");
    while (fgets(line, sizeof(line), file) != NULL)
    {
        strcat(response, line);
    }

    // Close the file
    fclose(file);

    return EXIT_SUCCESS;
}

int select_column_from_table(char *columnName, char *tableName, char *response)
{
    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", fileName);
        return EXIT_FAILURE;
    }

    char line[2048];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[2048];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Append the column value to the response
                strcat(response, columnValue);
                strcat(response, "\n");
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}

int select_column_from_table_where(char *columnName, char *tableName, char *condition, char *response)
{

    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", fileName);
        return EXIT_FAILURE;
    }

    char line[2048];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[2048];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Check if the condition is met
                if (strcmp(columnValue, condition) == 0)
                {
                    // Append the column value to the response
                    strcat(response, columnValue);
                    strcat(response, "\n");
                }
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return 0;
}
int update(char *table, char *setColumn, char *setNewValue,char *response){
    char fileName[2048 + 100];
    sprintf(fileName, "databases/%s/%s.csv", CURDATABASE, table);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", fileName);
        return EXIT_FAILURE;
    }
    FILE *tempFile = fopen("temp.csv", "w");
    if (tempFile == NULL)
    {
        fclose(file);
        sprintf(response, "Failed to create the temporary file\n");
        return EXIT_FAILURE;
    }
    char line[2048];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)
    fputs(line,tempFile);
    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columncount = 0;
    while (token != NULL)
    {
        if (strstr(token, setColumn) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columncount++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }
    char dataLine[1024];
    int updated = 0;
    while (fgets(dataLine, sizeof(dataLine), file) != NULL)
    {
        // Remove any existing newline characters from the data line
        char *newline = strchr(dataLine, '\n');
        if (newline != NULL)
        {
            *newline = '\0';
        }

        // Extract the column values from the data line
        char *column = strtok(dataLine, ",");
        char currentColumn[100];
        int columnIndex = 0;

        while (column != NULL)
        {
            strcpy(currentColumn, column);

            if (columnIndex == columncount)
            {
                // Update the column value
                strcpy(currentColumn, setNewValue);
                updated = 1;
            }

            fprintf(tempFile, "%s", currentColumn);

            column = strtok(NULL, ",");
            columnIndex++;

            if (column != NULL)
            {
                fprintf(tempFile, ",");
            }
        }

        // Write a new line
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    if (updated)
    {
        // Replace the original file with the temporary file
        if (remove(fileName) != 0)
        {
            sprintf(response, "Failed to remove the original file\n");
            return EXIT_FAILURE;
        }

        if (rename("temp.csv", fileName) != 0)
        {
            sprintf(response, "Failed to rename the temporary file\n");
            return EXIT_FAILURE;
        }
        sprintf(response, "Data updated successfully.\n");
    }
    else
    {
        // Remove the temporary file if no updates were made
        remove("temp.csv");
        sprintf(response, "No matching records found for the given conditions.\n");
    }
    return EXIT_SUCCESS;
}
int updateDatawhere(char *table, char *setColumn, char *setNewValue, char *whereColumn, char *whereValue, char *response)
{
  
    // Construct the file path
    char filePath[2048];
    sprintf(filePath, "databases/%s/%s.csv", CURDATABASE, table);

    // Open the CSV file for reading
    FILE *file = fopen(filePath, "r");
    if (file == NULL)
    {
        sprintf(response, "Failed to open the file '%s'\n", filePath);
        return EXIT_FAILURE;
    }

    // Create a temporary file for writing the updated data
    FILE *tempFile = fopen("temp.csv", "w");
    if (tempFile == NULL)
    {
        fclose(file);
        sprintf(response, "Failed to create the temporary file\n");
        return EXIT_FAILURE;
    }

    // Read and process the header line
    char headerLine[1024];
    fgets(headerLine, sizeof(headerLine), file);
    fputs(headerLine, tempFile);

    // Read and process each data line
    char dataLine[1024];
    int updated = 0;

    while (fgets(dataLine, sizeof(dataLine), file) != NULL)
    {
        // Remove any existing newline characters from the data line
        char *newline = strchr(dataLine, '\n');
        if (newline != NULL)
        {
            *newline = '\0';
        }

        // Extract the column values from the data line
        char *column = strtok(dataLine, ",");
        char currentColumn[100];
        int columnIndex = 0;

        while (column != NULL)
        {
            strcpy(currentColumn, column);

            if (strcmp(currentColumn, whereValue) == 0 && strcmp(setColumn, whereColumn) == 0)
            {
                // Update the column value
                strcpy(currentColumn, setNewValue);
                updated = 1;
            }

            fprintf(tempFile, "%s", currentColumn);

            column = strtok(NULL, ",");
            columnIndex++;

            if (column != NULL)
            {
                fprintf(tempFile, ",");
            }
        }

        // Write a new line
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    if (updated)
    {
        // Replace the original file with the temporary file
        if (remove(filePath) != 0)
        {
            sprintf(response, "Failed to remove the original file\n");
            return EXIT_FAILURE;
        }

        if (rename("temp.csv", filePath) != 0)
        {
            sprintf(response, "Failed to rename the temporary file\n");
            return EXIT_FAILURE;
        }
        sprintf(response, "Data updated successfully.\n");
    }
    else
    {
        // Remove the temporary file if no updates were made
        remove("temp.csv");
        sprintf(response, "No matching records found for the given conditions.\n");
    }
    return EXIT_SUCCESS;
}
int main(){
int auth=0;
 makedbdir();
 mkusers();
    char buffer[30030], msg[300303] = {0}, query[30030]={0};
    int server_fd, new_socket, vread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server started\n");

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
        perror("accept failed");
        exit(EXIT_FAILURE);
    }

    char user[1024]={0}, pswd[1024]={0};
while (1){
        memset(buffer, 0, sizeof(buffer));
        vread = read(new_socket, buffer, sizeof(buffer) - 1);
        memset(msg, 0, sizeof(msg));
        strcpy(query, buffer);
        if(strstr(buffer, "access") != NULL){ 
            strcpy(msg, "Login detected");
            char *token = strtok(query, " ");;
            token = strtok(NULL, " ");
            strcpy(user, token);
            token = strtok(NULL, " ");
            strcpy(pswd, token);
            if(strcmp(user, "Root") == 0 || checkusr(user)== 1){
                auth =1;
                printf("Welcome %s!\n", user);
            }else{
                printf("Authentification failed!\n"); 
            }
        }
        else if(strstr(query, "CREATE USER") != NULL){ 
            if(strcmp(user, "Root")==0){
                char newuser[1024], newpswd[1024];

                char *token = strtok(query, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, " ");
                strcpy(newuser, token);
                if (token != NULL) {
                    printf("New user : %s\n", newuser);
                }
                token = strtok(NULL, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, ";");
                strcpy(newpswd, token);
                if (token != NULL) {
                    printf("New user password: %s\n", newpswd);
                }
                if(checkusr(newuser)){
                    printf("This username is already exist!\n");
                    strcpy(msg, "This username is already exist");
                }else{
                    crusr(newuser, newpswd);
                    strcpy(msg, "Create username success");
                }
            }else{
                strcpy(msg, "Not root, create failed");
            }
        }else if(strstr(query, "USE") != NULL){
            char *tokBuf = strtok(query, " ");
        	tokBuf = strtok(NULL, " ");
        	char *database = strtok(tokBuf, ";");

        	if(checkperm(user, database) == 1){
        		strcpy(CURDATABASE, database);
                printf("Database Accessed\n");
                strcpy(msg, "Database accessed");
			}else{
				//tidak punya
				printf("Access denied\n");
                strcpy(msg, "No authorization upon database!");
			}

		}else if(strstr(query, "CREATE DATABASE") != NULL){ 
            char *tokBuf = strtok(query, " ");
        	tokBuf = strtok(NULL, " ");
        	tokBuf = strtok(NULL, " ");
        	char *database = strtok(tokBuf, ";");
            database[strlen(database)] = '\0';
        	mkdb(database);
            strcpy(msg, "Invoke create database");
            if(strcmp(user, "root") != 0){mkperm(user, database);} // jika pembuat bukan root
            mkperm("Root", database);
		}else if(strstr(query, "GRANT PERMISSION")){ 
           if(strcmp(user, "Root") == 0){   
                char *tokBuf = strtok(query, " ");
                tokBuf = strtok(NULL, " ");
                char *dbAcc = strtok(NULL, " ");
                tokBuf = strtok(NULL, " ");
                char *userAcc = strtok(NULL, ";");

                if(checkusr(userAcc)){
                    mkperm(userAcc, dbAcc);
                    sprintf(msg, "Grant Acess %s to %s", dbAcc, userAcc);
                }else{
                    printf("user does not exist\n");
                    strcpy(msg, "User  not exist!");
                }
                }
        }

        else if(strstr(query, "CREATE TABLE") != NULL){
            if(strcmp(CURDATABASE, "") != 0){ 
                char tableName[1024], attr[1024];
                char *token = strtok(query, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, "(");
                strcpy(tableName, token);
                token = strtok(NULL, ")");
                strcpy(attr, token);
                createTable(tableName, attr);
                strcpy(msg, "Invoke create table");
            }
            else{
                printf("Use a database first to create table\n");
                strcpy(msg, "Use a databse first!");
            }

		}else if(strstr(query, "DROP") != NULL){
            if(strstr(query, "DATABASE") != NULL){
                        char dropDB[1024];
                char *tokBuf = strtok(query, " ");
                tokBuf = strtok(NULL, " ");

                // Extract the nama database
                tokBuf = strtok(NULL, ";");
                strcpy(dropDB, tokBuf);
                if (tokBuf != NULL) {
                    printf("inserted drop database: %s\n", dropDB);
                }

                if(checkperm(user, dropDB) == 1){
                    dropdb(dropDB);
                    strcpy(msg, "Invoke drop database");  
                }else{
                    printf("%s has no right upon this %s database\n", user, dropDB);
                    strcpy(msg, "No authorization upon database!");
                }

            }
            else if(strstr(query, "TABLE") != NULL){
                if(strcmp(CURDATABASE, "") != 0){
                    char dropTB[1024];
                    char *tokBuf = strtok(query, " ");
                    tokBuf = strtok(NULL, " ");
                    tokBuf = strtok(NULL, ";");
                    strcpy(dropTB, tokBuf);
                    if (tokBuf != NULL) {
                        printf("inserted drop table: %s\n", dropTB);
                    }

                    dropTable(dropTB, CURDATABASE);
                    strcpy(msg, "Invoke drop table");       
                }else{
                    printf("Use a database first!\n");
                    strcpy(msg, "Use a databse first!");
                }

            }else if(strstr(query, "COLUMN") != NULL){
            if(strcmp(CURDATABASE, "") != 0){
                    char dropCol[1024], sourceTB[1024];
                    char *tokBuf = strtok(query, " ");
                    tokBuf = strtok(NULL, " ");

                    // Extract the nama database
                    tokBuf = strtok(NULL, " ");
                    strcpy(dropCol, tokBuf);
                    tokBuf = strtok(NULL, " ");
                    tokBuf = strtok(NULL, ";");
                    strcpy(sourceTB, tokBuf);

                    printf("inserted drop Column: %s from table: %s\n", dropCol, sourceTB);
                    removeColumn(sourceTB, dropCol);
                    strcpy(msg, "Invoke drop column");  
                }else{
                    printf("Use a database first!\n");
                    strcpy(msg, "Use a database first!");
                }

            }
          
        }
        else if(strstr(query, "INSERT INTO") != NULL){
            if(strcmp(CURDATABASE, "") != 0){ 
                char tableName[1024], attr[1024];
                char *token = strtok(query, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, "(");
                strcpy(tableName, token);
                token = strtok(NULL, ")");
                strcpy(attr, token);
                insertTable(tableName, attr);
                strcpy(msg, "Invoke insert table");
            }
            else{
                printf("Use a database first to insert\n");
                strcpy(msg, "Use a databse first!");
            }
		}
		else if(strstr(query, "DELETE FROM") != NULL && strstr(query, "WHERE") == NULL){
            if(strcmp(CURDATABASE, "") != 0){
                char tableName[1024];
                char *token = strtok(query, " ");
                token = strtok(NULL, " ");
                token = strtok(NULL, ";");
                strcpy(tableName, token);
                deletedata(tableName);
                strcpy(msg, "Invoke delete table data");
            }else{
                printf("Use a database first to delete");
                strcpy(msg, "Use a database first!");
            }
		}else if(strstr(query, "UPDATE") != NULL && strstr(query, "WHERE") == NULL){
	    	if(strcmp(CURDATABASE, "") != 0){
                char inpBuf[1024], table[1024], header[1024], value[1024];
                strcpy(inpBuf, query);
                 char *token = strtok(inpBuf, " ");
                 token = strtok(NULL, " ");
                 strcpy(table, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, "=");
                    strcpy(header, token);
                    token = strtok(NULL, ";");
                    strcpy(value, token);
                    printf("Update data from : %s\nkolom : %s\nvalue : %s\n", table, header, value);
                    update(table,header,value,msg);
	        }
		}else if(strstr(query, "DELETE FROM") != NULL){
	    	if(strcmp(CURDATABASE, "") != 0){
	        char inpBuf[1024], table[1024], column[1024], value[1024];
                strcpy(inpBuf, query);
                 char *token = strtok(inpBuf, " ");
                 token = strtok(NULL, " ");
                 token = strtok(NULL, " ");
                 strcpy(table, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, "=");
                    strcpy(column, token);
                    token = strtok(NULL, ";");
                    strcpy(value, token);
                    printf("Update data from : %s\nkolom : %s\nvalue : %s\n", table, column, value);
                    delete_from_table_where(table,column,value);
                    strcpy(msg, "Invoke delete table data");
	        }
		}else if(strstr(query, "UPDATE") != NULL){
	    	if(strcmp(CURDATABASE, "") != 0){
	     char inpBuf[1024], table[1024], header[1024], value[1024], wheader[1024], wvalue[1024];
                strcpy(inpBuf, query);
                 char *token = strtok(inpBuf, " ");
                 token = strtok(NULL, " ");
                 strcpy(table, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, "=");
                    strcpy(header, token);
                    token = strtok(NULL, " ");
                    strcpy(value, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, "=");
                    strcpy(wheader, token);
                    token = strtok(NULL, ";");
                    strcpy(wvalue, token);
                    printf("Update data from : %s\nkolom : %s\nvalue : %s\n", table, header, value);
                    updateDatawhere(table,header,value,wheader,wvalue,msg);
	        }
		}else if(strstr(query, "SELECT") != NULL){
	    	if(strcmp(CURDATABASE, "") != 0){
                char inpBuf[1024], table[1024], column[1024], value[1024];
                if(strstr(query, "*") != NULL){
                strcpy(inpBuf, query);
                 char *token = strtok(inpBuf, " ");
                 token = strtok(NULL, " ");
                 token = strtok(NULL, " ");
                 token = strtok(NULL, ";");
                 strcpy(table, token);
                 selectAll(table,msg);
                } else if(strstr(query, "WHERE") != NULL){
                strcpy(inpBuf, query);
                 char *token = strtok(inpBuf, " ");
                 token = strtok(NULL, " ");
                 strcpy(column, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, " ");
                    strcpy(table, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, "=");
                    token = strtok(NULL, ";");
                    strcpy(value, token);
                    printf("Update data from : %s\nkolom : %s\nvalue : %s\n", table, column, value);
                    select_column_from_table_where(column,table,value,msg);
                }else {
                    strcpy(inpBuf, query);
                    char *token = strtok(inpBuf, " ");
                    token = strtok(NULL, " ");
                    strcpy(column, token);
                    token = strtok(NULL, " ");
                    token = strtok(NULL, ";");
                    strcpy(table, token);
                    select_column_from_table(column,table,msg);
                }
	        }
	        else{
	            printf("Use a database first to create table\n");
	            strcpy(msg, "Use a databse first!");
	        }

        }else{
            strcpy(msg, "Unknown/invalid syntax!");
        }
        updatelog(user,buffer);
        send(new_socket, msg, strlen(msg), 0);
        fflush(stdout); 
}
return 0;
}
