#!/bin/bash
cd /home/rano/sisop-praktikum-fp-2023-bs-d06/database/
gcc database.c -o database  # Compile the C file

if [ $? -eq 0 ]; then
  echo "Compilation successful"
  ./database  # Run the compiled program
else
  echo "Compilation failed"
fi